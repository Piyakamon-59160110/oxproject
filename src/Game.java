/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Admin
 */
import java.util.Scanner;
public class Game {
    private static OX ox;
    private static Scanner kb = new Scanner(System.in);
    private static int col;
    private static int row;
    public static void main(String[] args) {
        showWelcome();
        showPlayer();
        ox = new OX();
        while (true) {
            printTable();
            inputColRow();
            if (ox.checkColWin(col,row)) {
                printTable();
                printWin();
                break;
            }
            if (ox.checkRowWin(col,row)) {
                printTable();
                printWin();
                break;
            }
            if (ox.checkEsWin(col,row)) {
                printTable();
                printWin();
                break;
            }
            if (ox.checkSsWin(col,row)) {
                printTable();
                printWin();
                break;
            }
            if (ox.isDraw()) {
                printTable();
                printDraw();
                break;
            }
            ox.switchPlayer();
        }
    }
    
    private static void showWelcome() {
        System.out.println("Welcome to OX Game :D");
    }
    private static void showPlayer() {
        System.out.println("Player 1 : X\nPlayer 2 : O");
    }
    private static void printDraw() {
        System.out.println("Congratulation : Draw!!!");
    }
    private static void printWin() {
        System.out.println("Congratulation : " + ox.getCurrentPlayer() + " Win!!!");
    }
    private static void inputColRow() {
        boolean canPut = true;
        do {
            System.out.println("Input " + ox.getCurrentPlayer() + " Col :");
            col = kb.nextInt();
            System.out.println("Input " + ox.getCurrentPlayer() + " Row :");
            row = kb.nextInt();
            canPut = ox.putColRow(col, row);
            if (!canPut) {
                System.out.println("Please input number between 0-2");
            }
        } while (!canPut); 
    }
    private static void printTable() {
        System.out.print(ox.getTableString());
    }    
}
class OX {
    String table[][] = {
            {" ","0","1","2"},
            {"0","-","-","-"},
            {"1","-","-","-"},
            {"2","-","-","-"},
    };
    private String currentPlayer;
    private int turnCount;
    public OX() {
        currentPlayer = "X";
        turnCount = 0;
    }
    public String getTableString() {
        String result = "";
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                result = result + table[i][j];
            }
            result = result + "\n";
        }
        return result;
    }
    public String getCurrentPlayer() {
        return currentPlayer;
    }
    public void switchPlayer() {
        if (currentPlayer.equals("X")) {
            currentPlayer = "O";
        } else {
            currentPlayer = "X";
        }
    }
    public boolean putColRow(int col, int row) {
        try {
            if (!table[row+1][col+1].equals("-")) {
                return false;
            }
            table[row+1][col+1] = currentPlayer;
        } catch (ArrayIndexOutOfBoundsException e) {
            return false;
        }
        turnCount++;

        return true;
    }
    public String getColRow(int col, int row) {
        if (col > 2 || col < 0 || row > 2 || row < 0) {
            return null;
        }
        return table[row+1][col+1];
    }
    public boolean checkColWin(int col, int row) {
        boolean colWin = true;
        for (int i = 0; i < 3; i++) {
            if (!table[i+1][col+1].equals(currentPlayer)) {
                colWin = false;
            }
        }
        if (colWin) {
            return true;
        }
        return false;
    }
    public boolean checkRowWin(int col, int row) {
        boolean rowWin = true;
        for (int i = 0; i < 3; i++) {
            if (!table[row+1][i+1].equals(currentPlayer)) {
                rowWin = false;
            }
        }
        if (rowWin) {
            return true;
        }
        return false;
    }
    public boolean checkEsWin(int col, int row) {
        boolean esWin = true;
        for (int i = 0; i < 3; i++) {
            if (!table[i+1][i+1].equals(currentPlayer)) {
                esWin = false;
            }
        }
        if (esWin) {
            return true;
        }
        return false;
    }
    public boolean checkSsWin(int col, int row) {
        boolean ssWin = true;
        for (int i = 0; i < 3; i++) {
            if (!table[i+1][3-i].equals(currentPlayer)) {
                ssWin = false;
            }
        }
        if (ssWin) {
            return true;
        }
        return false;
    }
    public int getTurnCount() {
        return turnCount;
    }
    public boolean isDraw() {
        if (turnCount == 9) {
            return true;
        }
        return false;
    }
}